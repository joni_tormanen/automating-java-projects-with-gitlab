package org.hsmith.medium;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WorkerClassTest {
    @Test
    void sum() {
        WorkerClass worker = new WorkerClass();
        assertEquals(2, worker.sum(1, 1));
    }
}